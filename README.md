## https://app.codility.com/programmers/lessons/  

- https://app.codility.com/programmers/lessons/1-iterations/  
- https://app.codility.com/programmers/lessons/2-arrays/  
- https://app.codility.com/programmers/lessons/3-time_complexity/  
- https://app.codility.com/programmers/lessons/4-counting_elements/  
- https://app.codility.com/programmers/lessons/5-prefix_sums/  
- https://app.codility.com/programmers/lessons/6-sorting/  
- https://app.codility.com/programmers/lessons/7-stacks_and_queues/
- https://app.codility.com/programmers/lessons/8-leader/


## Solutions
- https://funnelgarden.com/codility-solutions/  
- https://github.com/karimhamdanali/codility/tree/master/src/codility  
- https://chienchikao.blogspot.com/
- https://github.com/Mickey0521/Codility  

