package l6.sorting;

import java.util.Arrays;

/**
 * Determine whether a triangle can be built from a given set of edges.
 * <li>>A[P] + A[Q] > A[R],
 * <li>>A[Q] + A[R] > A[P],
 * <li>>A[R] + A[P] > A[Q].
 * <p>
 * The strategy is to sort the array of integers and then check from the end if previous 2 elements have sum > current element.
 */
public class Triangle {

  /**
   * Detected time complexity: O(N*log(N))
   * Total score: 93% -> 100%
   */
  public int solution(int[] A) {
    System.out.println("\n** START " + Arrays.toString(A));

    int result = 0;
    Arrays.sort(A);

    for (int i = 2; i < A.length; i++) {
      long sum = (long) A[i - 2] + A[i - 1]; // overflow problem - extend to long!
      if (sum > A[i]) {
        result = 1;
        break;
      }
    }

    System.out.println("** END " + result);
    return result;
  }

}
