package l6.sorting;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

/**
 * Compute number of distinct values in an array.
 */
public class Distinct {

  /**
   * Detected time complexity: O(N*log(N)) or O(N)
   * Task Score  100%
   * Correctness 100%
   * Performance 100%
   */
  public int solution(int[] A) {
    System.out.println("\n** START " + Arrays.toString(A));
    Set<Integer> distinctValues = new HashSet<>(A.length);
    for (int value : A) {
      distinctValues.add(value);
    }
    int result = distinctValues.size();
    System.out.println("** END " + result);
    return result;
  }

  /**
   * Detected time complexity: O(N*log(N)) or O(N)
   * Task Score  91%
   * Correctness 100%
   * Performance 66%
   */
  public int solution_stream(int[] A) {
    System.out.println("\n** START " + Arrays.toString(A));
    int result = (int) Arrays.stream(A)
      .distinct()
      .count();
    System.out.println("** END " + result);
    return result;
  }
}
