package l6.sorting;

import java.util.Arrays;

/**
 * Maximize A[P] * A[Q] * A[R] for any triplet (P, Q, R).
 */
public class MaxProductOfThree {

  /**
   * Detected time complexity:  O(N * log(N))
   * Task Score: 100%
   */
  public int solution(int[] A) {
    System.out.println("\n** START " + Arrays.toString(A));

    Arrays.sort(A);
    int max = A[A.length - 1];
    int max_2 = A[A.length - 2];
    int max_3 = A[A.length - 3];

    int neg_1 = A[0];
    int neg_2 = A[1];

    int prod_neg = neg_1 * neg_2;
    int prod_poz = max_2 * max_3;

    int result;
    if (max < 0) { // all negative numbers
      result = prod_poz * max;
      System.out.println("** END " + result);
      return result;
    }
    // maximum product when having big negative numbers
    if (prod_neg > prod_poz)
      result = max * prod_neg; // big negative numbers and small positive
    else
      result = max * prod_poz; // all positive numbers

    System.out.println("** END " + result);
    return result;
  }
}
