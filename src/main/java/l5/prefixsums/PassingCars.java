package l5.prefixsums;

import java.util.Arrays;

/**
 * Count the number of passing cars on the road.
 */
public class PassingCars {

  /**
   * Total score: 100%
   * O(N)
   */
  public int solution(int[] A) {
    int zeros = 0;
    int carPasses = 0;

    for (int value : A) {
      if (value == 0) {
        zeros++;
      } else if (value == 1) {
        // for every 1 - there will be an extra car pass for ALL the 0's that came before it
        carPasses += zeros;
        if (carPasses > 1_000_000_000) {
          return -1;
        }
      } else throw new RuntimeException("shouldn't reach here");
    }
    return carPasses;
  }

  /**
   * Detected time complexity: O(N ** 2)
   * Task Score: 50%
   * Correctness: 100%
   * Performance: 0%
   */
  public int solution_first(int[] input) {
    System.out.println("\n** START array: " + Arrays.toString(input));
    int result = 0;

    for (int i = 0; i < input.length; i++) {
      if (input[i] == 0) {
        // count the number of 1s
        int[] remainder = Arrays.copyOfRange(input, i + 1, input.length);
        result += Arrays.stream(remainder).filter(element -> element == 1).count();
      }
    }

    System.out.println("** END " + result);
    return result;
  }
}
