package l5.prefixsums;

import java.util.Arrays;

/**
 * Find the minimal average of any slice containing at least two elements.
 *
 * The key to solve this task is these two patterns:
 *
 * (1) There must be some slices, with length of two or three, having the minimal average value among all the slices.
 * (2) And all the longer slices with minimal average are built up with these 2-element and/or 3-element small slices.
 */
public class MinAvgTwoSlice {

  /**
   * O(N)
   *
   * Task Score  100%
   * Correctness 100%
   * Performance 100%
   */
  public int solution(int[] A) {
    System.out.println("\n** START " + Arrays.toString(A));

    double average = Integer.MAX_VALUE;
    int index = Integer.MAX_VALUE;

    // group by two or three elements
    for (int i = 0; i < A.length - 2; i++) {
      double avg2 = (A[i] + A[i + 1]) / 2.0;
      double avg3 = (A[i] + A[i + 1] + +A[i + 2]) / 3.0;
      if (avg2 < average) {
        average = avg2;
        index = i;
        System.out.println("/2 avg: " + average + ", index= " + index);
      }
      if (avg3 < average) {
        average = avg3;
        index = i;
        System.out.println("/3 avg: " + average + ", index= " + index);
      }
    }

    //check /2 for last two elements
    double avg2 = (A[A.length - 1] + A[A.length - 2]) / 2.0;
    if (avg2 < average) {
      average = avg2;
      index = A.length - 2;
      System.out.println("last check /2: " + average + ", index= " + index);
    }

    System.out.println("\n** END " + index);
    return index;
  }
}
