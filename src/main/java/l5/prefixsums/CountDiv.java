package l5.prefixsums;

/**
 * Compute number of integers divisible by k in range [a..b].
 */
public class CountDiv {

  /**
   * O(1)
   * Task Score: 100%
   */
  public int solution(int A, int B, int K) {
    System.out.println("\n** START range: " + A + " - " + B + " divisible by " + K);

    int result = (B / K) - (A / K) + (A % K == 0 ? 1 : 0);

    System.out.println("** END " + result);
    return result;
  }

  /**
   * Task Score: 75%
   * Correctness: 75%
   * Performance: 75%
   */
  public int solution_2(int A, int B, int K) {
    System.out.println("\n** START range: " + A + " - " + B + " divisible by " + K);
    if (A == B) {
      if (A % K == 0) {
        return 1;
      }
      return 0;
    }

    int result = (B - A) / K;
    result += 1;

    System.out.println("** END " + result);
    return result;
  }

  /**
   * O(B-A)
   * <p>
   * Task Score: 50%
   * Correctness: 100%
   * Performance: 0%
   */
  public int solution_1(int A, int B, int K) {
    System.out.println("\n** START range: " + A + " - " + B + " divisible by " + K);
    int result = 0;

    for (int i = A; i <= B; i++) {
      int modulo = i % K;
      //System.out.println(i + " % " + K + " = " + modulo);
      if (modulo == 0)
        result++;
    }

    System.out.println("** END " + result);
    return result;
  }

}
