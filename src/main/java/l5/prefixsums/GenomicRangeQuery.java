package l5.prefixsums;

import java.util.Arrays;

/**
 * Find the minimal nucleotide from a range of sequence DNA.
 */
public class GenomicRangeQuery {

  /**
   * O(N * M)
   * Task Score:  62%
   * Correctness: 100%
   * Performance: 0%
   */
  public int[] solution(String S, int[] P, int[] Q) {
    System.out.println("\n** START S: " + S + ", P: " + Arrays.toString(P) + ", Q: " + Arrays.toString(Q));

    int[] result = new int[P.length];
    for (int i = 0; i < P.length; i++) {
      String part = S.substring(P[i], Q[i] + 1);
      System.out.println("part: " + part);
      if (part.contains("A"))
        result[i] = 1;
      else if (part.contains("C"))
        result[i] = 2;
      else if (part.contains("G"))
        result[i] = 3;
      else if (part.contains("T"))
        result[i] = 4;
    }

    System.out.println("\n** END result: " + Arrays.toString(result));
    return result;
  }

  // https://rafal.io/posts/codility-genomic-range-query.html
  public int[] genome(String S, int[] P, int[] Q) {
    System.out.println("\n** START S: " + S + ", P: " + Arrays.toString(P) + ", Q: " + Arrays.toString(Q));

    int len = S.length();
    int[][] arr = new int[len][4];
    int[] result = new int[P.length];

    for (int i = 0; i < len; i++) {
      char c = S.charAt(i);
      if (c == 'A') arr[i][0] = 1;
      if (c == 'C') arr[i][1] = 1;
      if (c == 'G') arr[i][2] = 1;
      if (c == 'T') arr[i][3] = 1;
    }

    print2D(arr);

    // compute prefixes
    System.out.println("compute prefixes");
    for (int i = 1; i < len; i++) {
      for (int j = 0; j < 4; j++) {
        arr[i][j] += arr[i - 1][j];
      }
    }

    print2D(arr);

    for (int i = 0; i < P.length; i++) {
      int x = P[i];
      int y = Q[i];

      for (int a = 0; a < 4; a++) {
        int sub = 0;
        if (x - 1 >= 0)
          sub = arr[x - 1][a];
        if (arr[y][a] - sub > 0) {
          result[i] = a + 1;
          break;
        }
      }

    }
    System.out.println("\n** END result: " + Arrays.toString(result));
    return result;
  }

  public static void print2D(int[][] mat) {
    // Loop through all rows
    for (int[] row : mat)
      // converting each row as string
      // and then printing in a separate line
      System.out.println(Arrays.toString(row));
  }


}
