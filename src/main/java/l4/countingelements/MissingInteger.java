package l4.countingelements;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

/**
 * Write a function:
 * <p>
 * class Solution { public int solution(int[] A); }
 * <p>
 * that, given an array A of N integers, returns the smallest positive integer (greater than 0) that does not occur in A.
 * <p>
 * For example, given A = [1, 3, 6, 4, 1, 2], the function should return 5.
 * <p>
 * Given A = [1, 2, 3], the function should return 4.
 * <p>
 * Given A = [−1, −3], the function should return 1.
 * <p>
 * Write an efficient algorithm for the following assumptions:
 * <p>
 * N is an integer within the range [1..100,000];
 * each element of array A is an integer within the range [−1,000,000..1,000,000].
 */
public class MissingInteger {

  /**
   * 100%
   * <p>
   * O(N) or O(N * log(N))
   */
  public int solution(int[] A) {
    System.out.println("\n** START " + Arrays.toString(A));

    final Set<Integer> positiveNumbers = new HashSet<>();
    int max = 0;

    for (int value : A) {
      if (value <= 0)
        continue;
      positiveNumbers.add(value);
      if (value > max)
        max = value;
    }
    System.out.println("positiveNumbers=" + positiveNumbers);
    System.out.println("max= " + max);

    if (positiveNumbers.size() == 0)
      return 1;
    if (positiveNumbers.size() == 1) {
      Integer value = positiveNumbers.iterator().next();
      if (value == 1)
        return 2;
      return 1;
    }

    for (int i = 1; i <= max; i++) {
      if (positiveNumbers.contains(i) == false) {
        System.out.println("** END " + i);
        return i;
      }
    }

    System.out.println("** END " + (max + 1));
    return max + 1;
  }
}
