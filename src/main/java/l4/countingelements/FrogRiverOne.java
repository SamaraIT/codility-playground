package l4.countingelements;

import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * Find the earliest time when a frog can jump to the other side of a river.
 */
public class FrogRiverOne {

  /**
   * array index represents time, value is leaf position
   * <p>
   * O(N)
   */
  public int solution(int finishPosition, int[] input) {
    System.out.println("\n** START finishPosition=" + finishPosition + ", array: " + Arrays.toString(input));
    int result = -1;

    List<Boolean> path = Stream.generate(() -> false)
      .limit(finishPosition)
      .collect(Collectors.toList());
    System.out.println("Generated path.size= " + path.size());

    int foundAll = 0;
    for (int j = 0; j < input.length; j++) {
      int leafPostion = input[j] - 1;
      System.out.println("leafPosition= " + input[j]);
      if (path.get(leafPostion) == true)
        continue;
      else {
        path.set(leafPostion, true);
        foundAll++;
        if (foundAll == finishPosition) {
          System.out.println("Found last " + input[j] + ", on position " + j);
          result = j;
          break;
        }
      }
    }

    System.out.println("** END " + result);
    return result;
  }

  public int solution2(int X, int[] A) {
    Set requiredLeavesSet = new HashSet();
    for (int i = 1; i <= X; i++) {
      requiredLeavesSet.add(i);
    }

    Set currentLeavesSet = new HashSet();

    for (int p = 0; p < A.length; p++) {
      currentLeavesSet.add(A[p]);
      //keep adding to current leaves set until it's at least the same size as required leaves set
      if (currentLeavesSet.size() < requiredLeavesSet.size())
        continue;

      if (currentLeavesSet.containsAll(requiredLeavesSet)) {
        return p;
      }
    }
    return -1;
  }
}
