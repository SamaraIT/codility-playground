package l4.countingelements;

import java.util.Arrays;
import java.util.stream.IntStream;

/**
 * Calculate the values of counters after applying all alternating operations: increase counter by 1; set value of all counters to current maximum.
 */
public class MaxCounters {

  /**
   * 100 %
   * <p>
   * O(N + M)
   */
  public int[] solution(int N, int[] input) {
    System.out.println("*** START N=" + N + ", " + Arrays.toString(input));
    int[] counters = new int[N];
    int[] reset = new int[N];

    int maxNumber = 0;
    int lastResetNumber = 0;
    for (int value : input) {
      if (value > N) {
        // maxNumber all
        // reset counters
        lastResetNumber = lastResetNumber + maxNumber;
        maxNumber = 0;
        System.out.println("Max all, lastResetNumber " + lastResetNumber);
        System.arraycopy(reset, 0, counters, 0, counters.length);
      } else {
        // increase by 1
        System.out.println("Increasing position " + value);
        counters[value - 1] = counters[value - 1] + 1;
        if (counters[value - 1] > maxNumber)
          maxNumber = counters[value - 1];
        System.out.println("maxNumber: " + maxNumber);
      }
      System.out.println(Arrays.toString(counters));
    }

    // increase counters by last maximum reset number
    System.out.println("- increase counters by last maximum reset number: " + lastResetNumber);
    for (int i = 0; i < counters.length; i++) {
      counters[i] = counters[i] + lastResetNumber;

    }

    System.out.println("*** END " + Arrays.toString(counters));
    return counters;
  }

  /**
   * 77%
   * <p>
   * initialize int array without Stream; store max in separate variable
   */
  public int[] solution_second(int N, int[] input) {
    int[] counters = new int[N];

    int max = 0;
    for (int i = 0; i < input.length; i++) {
      int value = input[i];
      if (value > N) {
        // max all
        for (int j = 0; j < counters.length; j++) {
          counters[j] = max;
        }
      } else {
        // increase by 1
        counters[value - 1] = counters[value - 1] + 1;
        if (counters[value - 1] > max)
          max = counters[value - 1];
      }
    }
    return counters;
  }

  /**
   * 44%
   * <p>
   * O(N * M)
   */
  public int[] solution_first(int N, int[] input) {
    System.out.println("*** START N=" + N + ", " + Arrays.toString(input));
    int[] counters = IntStream.generate(() -> 0)
      .limit(N).toArray();
    System.out.println("counters.length= " + Arrays.toString(counters));

    for (int i = 0; i < input.length; i++) {
      int value = input[i];
      if (value > N) {
        System.out.println("Max all counters");
        int max = Arrays.stream(counters).max().getAsInt();
        for (int j = 0; j < counters.length; j++) {
          counters[j] = max;
        }
      } else {
        System.out.println("Increasing position " + value);
        counters[value - 1] = counters[value - 1] + 1;
      }
      System.out.println(Arrays.toString(counters));
    }
    System.out.println("*** END " + Arrays.toString(counters));
    return counters;
  }
}
