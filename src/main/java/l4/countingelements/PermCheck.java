package l4.countingelements;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

/**
 * Check whether array A is a permutation.
 */
public class PermCheck {

  /**
   * O(N) or O(N * log(N))
   *
   * 100 %
   */
  public int solution(int[] input) {
    Set<Integer> expectedSet = new HashSet<>();
    Set<Integer> inputSet = new HashSet<>();

    // using 1 loop instead 2, when using streams
    for (int i = 0; i < input.length; i++) {
      inputSet.add(input[i]);
      expectedSet.add(i + 1);
    }

    int result = 0;
    if (inputSet.containsAll(expectedSet))
      result = 1;

    System.out.println("** END result=" + result);
    return result;
  }

  /**
   * O(N) or O(N * log(N)) or O(N ** 2)
   *
   * 83%
   *
   * Problem was when 2 sets are being created using streams - those are 2 for loops
   */
  public int solution_first(int[] input) {
    System.out.println("\n** START " + Arrays.toString(input));

    Set<Integer> expectedSet = IntStream.rangeClosed(1, input.length)
      .boxed()
      .collect(Collectors.toSet());
    System.out.println("Generated path.size= " + expectedSet.size());

    Set<Integer> inputSet = Arrays.stream(input)
      .boxed()
      .collect(Collectors.toSet());

    int result = 0;
    if (inputSet.containsAll(expectedSet))
      result = 1;

    System.out.println("** END result=" + result);
    return result;
  }
}
