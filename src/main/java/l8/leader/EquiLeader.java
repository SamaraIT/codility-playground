package l8.leader;

import java.util.Arrays;
import java.util.Objects;

/**
 * Find the index S such that the leaders of the sequences A[0], A[1], ..., A[S] and A[S + 1], A[S + 2], ..., A[N - 1] are the same.
 */
public class EquiLeader {

  /**
   * 100%
   * O(N)
   */
  public int solution(int[] A) {
    System.out.println("\n** START " + Arrays.toString(A));
    if (Objects.isNull(A) || A.length == 0)
      return 0;

    // find a leader and count them
    int[] B = new int[A.length];
    System.arraycopy(A, 0, B, 0, A.length);
    Arrays.sort(B);
    boolean leaderFound = false;
    int leader = B[B.length / 2];
    int leaderCount = 0;
    for (int i = 0; i < A.length; i++) {
      if (A[i] == leader)
        leaderCount++;
      if (leaderCount > (B.length / 2)) {
        leaderFound = true;
      }
    }
    if (leaderFound == false) {
      System.out.println("No leader found - abort ");
      return 0;
    }
    System.out.println("Leader= " + leader + ", leaderCount: " + leaderCount);

    int left_leaders_count = 0;
    int equiLeader = 0;
    for (int i = 0; i < A.length; i++) {
      if (A[i] == leader)
        left_leaders_count++;

      int halfSize = (i + 1) / 2;
      if (left_leaders_count > halfSize) { // leader on left side
        halfSize = (A.length - i - 1) / 2;
        if (leaderCount - left_leaders_count > halfSize) { //check right side
          equiLeader++;
        }
      }
    }

    System.out.println("** END equiLeader: " + equiLeader);
    return equiLeader;
  }
}
