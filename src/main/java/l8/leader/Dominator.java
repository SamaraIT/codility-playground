package l8.leader;

import java.util.Arrays;
import java.util.Objects;

/**
 * Find an index of an array such that its value occurs at more than half of indices in the array.
 */
public class Dominator {

  /**
   * O(N*log(N)) or O(N)
   * Task Score  100%
   */
  public int solution(int[] A) {
    System.out.println("\n** START " + Arrays.toString(A));
    if (Objects.isNull(A) || A.length == 0)
      return -1;

    int[] B = new int[A.length];
    System.arraycopy(A, 0, B, 0, A.length);
    Arrays.sort(B);

    int halfSize = B.length / 2;
    int leader = B[halfSize];
    int counter = 0;
    for (int i = 0; i < A.length; i++) {
      if (A[i] == leader)
        counter++;
      if (counter > halfSize) {
        System.out.println("** END leader=" + leader + ", index=" + i);
        return i;
      }
    }

    System.out.println("** END no leader");
    return -1;
  }
}
