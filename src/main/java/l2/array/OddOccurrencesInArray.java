package l2.array;

import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * Find value that occurs in odd number of elements.
 * <p>
 * Detected time complexity: O(N**2)
 * <p>
 * Detected time complexity: O(N) or O(N*log(N))   - removed parallel stream
 * <p>
 * A non-empty array A consisting of N integers is given. The array contains an odd number of elements, and each element of the array can be paired with another element that has the same value, except for one element that is left unpaired.
 * <p>
 * For example, in array A such that:
 * <p>
 * A[0] = 9  A[1] = 3  A[2] = 9
 * A[3] = 3  A[4] = 9  A[5] = 7
 * A[6] = 9
 * the elements at indexes 0 and 2 have value 9,
 * the elements at indexes 1 and 3 have value 3,
 * the elements at indexes 4 and 6 have value 9,
 * the element at index 5 has value 7 and is unpaired.
 * Write a function:
 * <p>
 * class Solution { public int solution(int[] A); }
 * <p>
 * that, given an array A consisting of N integers fulfilling the above conditions, returns the value of the unpaired element.
 * <p>
 * For example, given array A such that:
 * <p>
 * A[0] = 9  A[1] = 3  A[2] = 9
 * A[3] = 3  A[4] = 9  A[5] = 7
 * A[6] = 9
 * the function should return 7, as explained in the example above.
 * <p>
 * Write an efficient algorithm for the following assumptions:
 * <p>
 * N is an odd integer within the range [1..1,000,000];
 * each element of array A is an integer within the range [1..1,000,000,000];
 * all but one of the values in A occur an even number of times.
 */
public class OddOccurrencesInArray {

  public static void main(String[] args) {

    int result = solution(new int[]{3, 3, 9, 7, 7});
    assert result == 9;

    result = solution(new int[]{3, 7, 9, 7, 3});
    assert result == 9;

    result = solution(new int[]{3, 3, 9, 3, 3});
    assert result == 9;

    result = solution(new int[]{1, 1, 3, 1, 3});
    assert result == 1;

// big data generator
    Random rand = new Random();
    List<Integer> integers = Stream.generate(() -> rand.nextInt((999_999_999 - 1) + 1) + 1)
      .limit(999_999 / 2)
      .collect(Collectors.toList());
    List<Integer> i2 = new ArrayList();
    i2.addAll(integers);
    i2.addAll(integers);
    i2.add(1_000_000_000);

    int[] array = i2.stream()
      .mapToInt(i -> i).toArray();

    long start = System.nanoTime();
    result = solution(array);
    long end = System.nanoTime();
    long diff = end - start;
    System.out.println("Time: " + diff / 1000000000.0 + " seconds");
    assert result == 1_000_000_000;
  }

  static int solution(int[] A) {
    if (A.length > 100)
      System.out.println("START array.length= " + A.length);
    else
      System.out.println("START " + Arrays.toString(A));

    Map<Integer, Long> map = Arrays.stream(A)
      //.parallel()   // without: Time: 0.4469854 seconds, with parallel: Time: 0.55249 seconds
      .mapToObj(i -> Integer.valueOf(i))
      .collect(Collectors.groupingBy(
        i -> i, Collectors.counting()));

    if (A.length <= 100)
      System.out.println("made temp map: " + map);


    for (Map.Entry<Integer, Long> e : map.entrySet()) {
      if (e.getValue() % 2 != 0) {
        System.out.println("Found oddKey: " + e.getKey());
        return e.getKey();
      }
    }
/*    Integer oddKey = map.entrySet().stream()
      .filter(integerLongEntry -> integerLongEntry.getValue() % 2 == 1)
      .map(integerLongEntry -> integerLongEntry.getKey())
      .findAny().orElseThrow();
    System.out.println("Found oddKey: " + oddKey);*/
    throw new RuntimeException("no odd entry in array ?!");
  }
}
