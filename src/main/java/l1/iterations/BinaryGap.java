package l1.iterations;

/**
 * A binary gap within a positive integer N is any maximal sequence of consecutive zeros that is surrounded by ones at both ends in the binary representation of N.
 * <p>
 * For example, number 9 has binary representation 1001 and contains a binary gap of length 2. The number 529 has binary representation 1000010001 and contains two binary gaps: one of length 4 and one of length 3. The number 20 has binary representation 10100 and contains one binary gap of length 1. The number 15 has binary representation 1111 and has no binary gaps. The number 32 has binary representation 100000 and has no binary gaps.
 * <p>
 * Write a function:
 * <p>
 * class Solution { public int solution(int N); }
 * <p>
 * that, given a positive integer N, returns the length of its longest binary gap. The function should return 0 if N doesn't contain a binary gap.
 * <p>
 * For example, given N = 1041 the function should return 5, because N has binary representation 10000010001 and so its longest binary gap is of length 5. Given N = 32 the function should return 0, because N has binary representation '100000' and thus no binary gaps.
 * <p>
 * Write an efficient algorithm for the following assumptions:
 * <p>
 * N is an integer within the range [1..2,147,483,647].
 */
public class BinaryGap {

  public static void main(String[] args) {
    System.out.println("START - enable -ea option");
    assert binaryGap(9) == 2;     // 1001
    assert binaryGap(529) == 4;
    assert binaryGap(1041) == 5;
    assert binaryGap(15) == 0;
    assert binaryGap(32) == 0;
    assert binaryGap(20) == 1;
    assert binaryGap(1162) == 3;  // 10010001010
    assert binaryGap(51712) == 2; // 110010100000000_2
    assert binaryGap(1073741825) == 29; // 110010100000000_2
    System.out.println("END");
  }

  /**
   * given a positive integer N, returns the length of its longest binary gap
   */
  static int binaryGap(int N) {
    System.out.println("\n** binaryGap START " + N);
    String binaryString = Integer.toBinaryString(N);
    System.out.println("binaryString: " + binaryString);

    boolean hasStart = false;
    int counter = 0;
    int binaryGap = 0;
    for (int i = 0; i < binaryString.length(); i++) {
      if (binaryString.charAt(i) == '1' && !hasStart) {
        System.out.println("Found starting 1");
        hasStart = true;
        continue;
      }
      if (binaryString.charAt(i) == '0') {
        System.out.println("Increasing counter");
        counter++;
      } else if (binaryString.charAt(i) == '1' && hasStart) {
        System.out.println("Found ending 1, counter: " + counter);
        if (counter > binaryGap)
          binaryGap = counter;
        counter = 0;
      }
    }

    System.out.println("binaryGap END " + binaryGap);
    return binaryGap;
  }

}
