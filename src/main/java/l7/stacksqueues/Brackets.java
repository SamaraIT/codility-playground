package l7.stacksqueues;

import java.util.Stack;

/**
 * Determine whether a given string of parentheses (multiple types) is properly nested.
 * string S consists only of the following characters: "(", "{", "[", "]", "}" and/or ")".
 * For example, given S = "{[()()]}", the function should return 1 and given S = "([)()]", the function should return 0, as explained above.
 */
public class Brackets {

  /**
   * 100%
   * O(N)
   */
  public int solution(String S) {
    System.out.println("\n** START " + S);

    if (S.isEmpty())
      return 1;

    Stack<Character> stack = new Stack<>();
    for (char item : S.toCharArray()) {
      if (isOpeningBracket(item))
        stack.push(item);
      else {
        if (stack.isEmpty()) // more closing brackets
          return 0;
        Character pop = stack.pop();
        if (isBracketMatch(pop, item) == false) {
          return 0;
        }
      }
    }

    System.out.println("stack: " + stack);
    int result = 1;
    if (stack.isEmpty() == false) { // more opening brackets are present
      result = 0;
    }

    System.out.println("** END " + result);
    return result;
  }

  boolean isOpeningBracket(char bracket) {
    if (bracket == '[' || bracket == '(' || bracket == '{') {
      return true;
    }
    return false;
  }

  boolean isBracketMatch(char openingBracket, char closingBracket) {
    if (openingBracket == '[' && closingBracket == ']') return true;
    if (openingBracket == '(' && closingBracket == ')') return true;
    if (openingBracket == '{' && closingBracket == '}') return true;

    return false;
  }
}
