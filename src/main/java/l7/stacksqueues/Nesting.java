package l7.stacksqueues;

import java.util.Stack;

/**
 * Determine whether a given string of parentheses (single type) is properly nested.
 */
public class Nesting {

  /**
   * 100%
   * O(N)
   */
  public int solution(String S) {
    System.out.println("\n** START " + S);
    if (S.isEmpty())
      return 1;
    if (S.startsWith(")"))
      return 0;

    Stack<Character> stack = new Stack<>();
    for (char c : S.toCharArray()) {
      if (c == '(')
        stack.push(c);
      else if (c == ')') {
        if (stack.isEmpty())
          return 0;
        stack.pop();
      }
    }

    int result = 1;
    if (stack.isEmpty() == false) { // more opening brackets are present
      result = 0;
    }
    System.out.println("** END " + result);
    return result;
  }
}
