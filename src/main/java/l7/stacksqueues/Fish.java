package l7.stacksqueues;

import java.util.Arrays;
import java.util.Stack;

/**
 * N voracious fish are moving along a river. Calculate how many fish are alive.
 * 0 represents a fish flowing upstream,
 * 1 represents a fish flowing downstream
 */
public class Fish {

  // https://github.com/Mickey0521/Codility/blob/master/Fish.java
  public int solution(int[] A, int[] B) {
    System.out.println("\n** START " + Arrays.toString(A) + ", B= " + Arrays.toString(B));

    // special case: no fish
    if (A.length == 0)
      return 0;

    // main idea: use "stack" to store the fishes with B[i]==1
    // that is, "push" the downstream fishes into "stack"
    // note: "push" the Size of the downstream fish
    Stack<Integer> st = new Stack<>();
    int numAlive = A.length;

    for (int i = 0; i < A.length; i++) {

      // case 1; for the fish going to downstream
      // push the fish to "stack", so we can keep them from the "last" one
      if (B[i] == 1) {
        st.push(A[i]); // push the size of the downstream fish
      }
      // case 2: for the fish going upstream
      // check if there is any fish going to downstream
      if (B[i] == 0) {
        while (!st.isEmpty()) {
          // if the downstream fish is bigger (eat the upstream fish)
          if (st.peek() > A[i]) {
            numAlive--;
            break;      // the upstream fish is eaten (ending)
          }
          // if the downstream fish is smaller (eat the downstream fish)
          else if (st.peek() < A[i]) {
            numAlive--;
            st.pop();   // the downstream fish is eaten (not ending)
          }
        }
      }
    }

    return numAlive;
  }

  /**
   * Task Score 37%
   * Correctness 50%
   * Performance 25%
   */
  public int solution_first(int[] fishesSize, int[] fishesDirection) {
    System.out.println("\n** START " + Arrays.toString(fishesSize) + ", B= " + Arrays.toString(fishesDirection));

    Stack<Integer> sizeStack = new Stack<>();
    Stack<Integer> directionStack = new Stack<>();

    sizeStack.push(fishesSize[0]);
    directionStack.push(fishesDirection[0]);

    int previousSize;
    int previousDirection;
    int currentDirection;
    int currentSize;
    for (int i = 1; i < fishesSize.length; i++) {
      previousDirection = directionStack.pop();
      previousSize = sizeStack.pop();

      currentDirection = fishesDirection[i];
      currentSize = fishesSize[i];

      // fish move in opposite directions
      if (previousDirection != currentDirection) {
        if (previousDirection == 0 && currentDirection == 1) {
          // move both fishes back to stack
          directionStack.push(previousDirection);
          sizeStack.push(previousSize);
          directionStack.push(currentDirection);
          sizeStack.push(currentSize);
        } else { //  the larger fish eats the smaller one
          if (previousSize > fishesSize[i]) {
            directionStack.push(previousDirection);
            sizeStack.push(previousSize);
          } else {
            directionStack.push(currentDirection);
            sizeStack.push(currentSize);
          }
        }
      } else {
        // move both fishes back to stack
        directionStack.push(previousDirection);
        sizeStack.push(previousSize);
        directionStack.push(fishesDirection[i]);
        sizeStack.push(fishesSize[i]);
      }

    }
    System.out.println("* sizeStack " + sizeStack);
    System.out.println("* directionStack " + directionStack);

    System.out.println("** END " + directionStack.size());
    return directionStack.size();
  }

}
