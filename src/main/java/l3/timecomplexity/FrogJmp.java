package l3.timecomplexity;

/**
 * A small frog wants to get to the other side of the road. The frog is currently located at position X and wants to get to a position greater than or equal to Y. The small frog always jumps a fixed distance, D.
 * <p>
 * Count the minimal number of jumps that the small frog must perform to reach its target.
 * <p>
 * Write a function:
 * <p>
 * class Solution { public int solution(int X, int Y, int D); }
 * <p>
 * that, given three integers X, Y and D, returns the minimal number of jumps from position X to a position equal to or greater than Y.
 * <p>
 * For example, given:
 * <p>
 * X = 10
 * Y = 85
 * D = 30
 * the function should return 3, because the frog will be positioned as follows:
 * <p>
 * after the first jump, at position 10 + 30 = 40
 * after the second jump, at position 10 + 30 + 30 = 70
 * after the third jump, at position 10 + 30 + 30 + 30 = 100
 * Write an efficient algorithm for the following assumptions:
 * <p>
 * X, Y and D are integers within the range [1..1,000,000,000];
 * X ≤ Y.
 */
public class FrogJmp {
  public static void main(String[] args) {
    int solution = solution(10, 85, 30);
    assert solution == 3;

    solution = solution(100, 85, 30);
    assert solution == 0;

    solution = solution(1, 5, 2);
    assert solution == 2;

    solution = solution(3, 999111321, 7);
    System.out.println(solution);
    assert solution == 142730189;
  }

  /**
   * https://funnelgarden.com/frogjmp-codility-solution/
   * <p>
   * Detected time complexity: O(1)
   */
  static int solution(int X, int Y, int D) {
    System.out.println("\nSTART startingPoint=" + X + ", destinationPoint=" + Y + ", jump=" + D);
    if (X >= Y)
      return 0;
    int distanceToJump = Y - X;
    int jumpsRequired = distanceToJump / D;
    System.out.println("jumpsRequired="+jumpsRequired);
    if (distanceToJump % D != 0) {
      jumpsRequired++; //only add extra jump if remainder exists
    }
    System.out.println("END jumps= " + jumpsRequired);
    return jumpsRequired;
  }

  /**
   * Detected time complexity: O(Y-X)
   */
  static int solution_slow(int X, int Y, int D) {
    System.out.println("\nSTART startingPoint=" + X + ", destinationPoint=" + Y + ", jump=" + D);
    if (X >= Y)
      return 0;

    int result = X;
    int jumps = 0;

    while (true) {
      jumps++;
      result = result + D;
      if (result >= Y) {
        System.out.println("END jumps= " + jumps);
        return jumps;
      }
    }
  }
}
