package l3.timecomplexity;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

/**
 * Find the missing element in a given permutation.
 */
public class PermMissingElem {

  /**
   * Detected time complexity: O(N) or O(N * log(N))
   */
  public int solution_binarySearch(int[] input) {
    assert (input != null);

    System.out.println("** START " + Arrays.toString(input));
    int length = input.length;
    if (length == 0) {
      return 1;
    }

    Arrays.sort(input);

    for (int ref = 1; ref <= length + 1; ref++) {
      System.out.println("looking for " + ref);
      int binarySearch = Arrays.binarySearch(input, ref);
      System.out.println("binarySearch: " + binarySearch);
      if (binarySearch < 0)
        return ref;
    }

    throw new RuntimeException("opsssss");
  }

  /**
   * Detected time complexity: O(N) or O(N * log(N))
   */
  public int solution(int[] input) {
    int max = input.length + 1;

    Set incompleteSet = new HashSet();
    for (int i = 0; i < input.length; i++) {
      incompleteSet.add(input[i]);
    }

    for (int i = 1; i < max + 1; i++) {
      if (!incompleteSet.contains(i)) {
        return i;
      }
    }
    throw new RuntimeException("opsssss");
  }

  /**
   * Detected time complexity: O(N ** 2)
   */
  public int solution_bad(int[] input) {
    assert (input != null);

    System.out.println("** START " + Arrays.toString(input));
    int length = input.length;
    if (length == 0) {
      return 1;
    }

    for (int ref = 1; ref <= length + 1; ref++) {
      System.out.println("looking for " + ref);
      for (int inp = 0; inp < input.length; inp++) {
        System.out.println("looping over input " + inp);
        if (input[inp] == ref) {
          System.out.println("Found " + ref + " -> break");
          break;
        }
        if (inp == input.length - 1) {
          System.out.println("** END of processing input - element is not found: " + ref);
          return ref;
        }
      }
    }

    throw new RuntimeException("opsssss");
  }

}
