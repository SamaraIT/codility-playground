package l3.timecomplexity;

import java.util.Arrays;

/**
 * Minimize the value |(A[0] + ... + A[P-1]) - (A[P] + ... + A[N-1])|.
 */
public class TapeEquilibrium {

  /**
   * O(N)
   * <p>
   * score=84%-100%
   * <p>
   * The main strategy is to first calculate the sum of the entire array
   * and then use a running sum for each array index to calculate the sum of both sub-arrays at each index.
   */
  public int solution(int[] input) {

    int sumAllElements = Arrays.stream(input).sum();

    int minDifference = Integer.MAX_VALUE;
    int currentDifference;
    long sumFirstPart = 0;
    long sumSecondPart;

    for (int p = 0; p < input.length - 1; p++) {
      sumFirstPart += input[p];
      sumSecondPart = sumAllElements - sumFirstPart;

      currentDifference = (int) Math.abs(sumFirstPart - sumSecondPart);
      minDifference = Math.min(currentDifference, minDifference);
      if (minDifference == 0) {
        return minDifference;
      }
    }
    return minDifference;
  }

  /**
   * O(N * N)
   * <p>
   * score=46%
   */
  public int solution_bad(int[] input) {
//    System.out.println("** START " + Arrays.toString(input));
    Arrays.copyOfRange(input, 1, input.length);
    int slice = 1;
    int minimum = 1_000;
    while (slice < input.length) {
      int[] a = Arrays.copyOfRange(input, 0, slice);
      int[] b = Arrays.copyOfRange(input, slice, input.length);
//      System.out.println("\n slice= " + slice);
//      System.out.println("a: " + Arrays.toString(a));
//      System.out.println("b: " + Arrays.toString(b));
      slice++;

      int sumA = Arrays.stream(a).sum();
      int sumB = Arrays.stream(b).sum();
      int abs = Math.abs(sumA - sumB);
//      System.out.println("abs= " + abs);
      if (abs == 0)
        return 0;
      if (abs < minimum) {
//        System.out.println("new minimum: " + abs);
        minimum = abs;
      }
    }
//    System.out.println("** END " + minimum);
    return minimum;
  }
}
