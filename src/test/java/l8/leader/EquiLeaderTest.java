package l8.leader;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.MethodSource;

class EquiLeaderTest {
  EquiLeader equiLeader = new EquiLeader();

  @Test
  void solution() {
    Assertions.assertEquals(1, equiLeader.solution(new int[]{3, 3}));
    Assertions.assertEquals(2, equiLeader.solution(new int[]{3, 3, 3}));
    Assertions.assertEquals(3, equiLeader.solution(new int[]{3, 3, 3, 3}));
    Assertions.assertEquals(2, equiLeader.solution(new int[]{4, 3, 4, 4, 4, 2}));
    Assertions.assertEquals(3, equiLeader.solution(new int[]{1, 2, 1, 1, 2, 1}));
  }

  @ParameterizedTest(name = "{index} => A={0}, result={1}")
  @MethodSource("createData1")
  public void verifySolution(int[] A, int result) {
    Assertions.assertEquals(result, equiLeader.solution(A));
  }

  static Object [][] createData1() {
    return new Object [][] {
      //new Object [] { new int [] { 4, 3, 4, 4, 4, 2 }, 2 },
      new Object [] { new int [] { 1, 5, 1, 5, 5, 5, 5, 5 }, 3 },
      new Object [] { new int [] { 1, 1, 1, 1, 1, 1 }, 5 },
      new Object [] { new int [] { 4, 4, 2, 5, 3, 4, 4, 4 }, 3 },

      new Object [] { new int [] { 1, 2, 3  }, 0 },
      new Object [] { new int [] { 1000000000  }, 0 },
      new Object [] { new int [] { 1000, 1  }, 0 },
      new Object [] { new int [] { 0, 0  }, 1 },
    };
  }
}
