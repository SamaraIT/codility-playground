package l8.leader;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

class DominatorTest {

  Dominator dominator = new Dominator();

  @Test
  void solution() {
    Assertions.assertEquals(2, dominator.solution(new int[]{3, 2, 3}));
    Assertions.assertEquals(0, dominator.solution(new int[]{3}));
    Assertions.assertEquals(-1, dominator.solution(new int[]{1, 2, 3}));
    Assertions.assertEquals(7, dominator.solution(new int[]{3, 4, 3, 2, 3, -1, 3, 3}));
    Assertions.assertEquals(-1, dominator.solution(new int[]{}));
  }

}
