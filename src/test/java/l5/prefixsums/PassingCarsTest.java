package l5.prefixsums;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.MethodSource;

class PassingCarsTest {
  PassingCars passingCars = new PassingCars();

  @Test
  void solution() {
    Assertions.assertEquals(5, passingCars.solution(new int[]{0, 1, 0, 1, 1}));
    Assertions.assertEquals(5, passingCars.solution(new int[]{0, 1, 0, 1, 1, 0}));
    Assertions.assertEquals(0, passingCars.solution(new int[]{0, 0, 0, 0, 0}));
    Assertions.assertEquals(0, passingCars.solution(new int[]{1, 1}));
  }

  @ParameterizedTest(name = "{index} => A={0}, result={1}")
  @MethodSource("createData1")
  public void verifySolution(int[] A, int result) {
    Assertions.assertEquals(result, passingCars.solution(A));
  }


  public static Object[][] createData1() {
    return new Object[][]{
      new Object[]{new int[]{0, 1, 0, 1, 1}, 5},
      new Object[]{new int[]{1, 0, 0, 1, 0, 1}, 5},
      new Object[]{new int[]{0, 1, 0, 1, 0, 1, 0, 1, 0, 1}, 15},
      new Object[]{new int[]{1, 1, 0, 1}, 1},
      new Object[]{new int[]{0, 1, 1, 1}, 3},
      new Object[]{new int[]{0, 1, 1, 1, 0}, 3},
      new Object[]{new int[]{0, 1, 1, 1, 0, 1}, 5},
      new Object[]{new int[]{0, 1, 1, 1, 0, 1, 1}, 7},
      new Object[]{new int[]{0, 1, 1, 1, 0, 1, 1, 1}, 9},
      new Object[]{new int[]{0, 1, 1, 1, 0, 1, 1, 1, 0}, 9},
      new Object[]{new int[]{0, 1, 1, 1, 0, 1, 1, 1, 0, 0}, 9},
      new Object[]{new int[]{0, 1, 1, 1, 0, 1, 1, 1, 0, 0, 1}, 13},

      //no passing cars
      new Object[]{new int[]{1}, 0},
      new Object[]{new int[]{0}, 0},
      new Object[]{new int[]{0, 0}, 0},
      new Object[]{new int[]{1, 1}, 0},
      new Object[]{new int[]{1, 1, 0}, 0},

    };
  }
}
