package l5.prefixsums;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

class GenomicRangeQueryTest {

  GenomicRangeQuery query = new GenomicRangeQuery();

  @Test
  void solution() {
    Assertions.assertArrayEquals(new int[]{2, 4, 1}, query.solution("CAGCCTA", new int[]{2, 5, 0}, new int[]{4,5,6}));
  }

  @Test
  void genome() {
    Assertions.assertArrayEquals(new int[]{2, 4, 1}, query.genome("CAGCCTA", new int[]{2, 5, 0}, new int[]{4,5,6}));
  }
}
