package l5.prefixsums;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

class CountDivTest {

  CountDiv countDiv = new CountDiv();

  @Test
  void solution() {
    Assertions.assertEquals(3, countDiv.solution(6, 11, 2));
    Assertions.assertEquals(4, countDiv.solution(6, 12, 2));
    Assertions.assertEquals(1, countDiv.solution(11, 13, 2));

    Assertions.assertEquals(20, countDiv.solution(11, 345, 17));

    Assertions.assertEquals(1, countDiv.solution(10, 10, 5));
    Assertions.assertEquals(0, countDiv.solution(10, 10, 7));
    Assertions.assertEquals(0, countDiv.solution(10, 10, 20));
  }
}
