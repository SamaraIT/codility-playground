package l5.prefixsums;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

class MinAvgTwoSliceTest {

  MinAvgTwoSlice minAvgTwoSlice = new MinAvgTwoSlice();

  @Test
  void solution() {
    Assertions.assertEquals(1, minAvgTwoSlice.solution(new int[]{4, 2, 2, 5, 1, 5, 8}));
  }
}
