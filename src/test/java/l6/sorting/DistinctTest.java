package l6.sorting;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

class DistinctTest {

  final Distinct distinct = new Distinct();

  @Test
  void solution() {
    Assertions.assertEquals(3, distinct.solution(new int[]{2, 1, 1, 2, 3, 1}));
  }
}
