package l6.sorting;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

class MaxProductOfThreeTest {
  MaxProductOfThree maxProductOfThree = new MaxProductOfThree();

  @Test
  void solution() {
    Assertions.assertEquals(60, maxProductOfThree.solution(new int[]{-3, 1, 2, -2, 5, 6}));
    Assertions.assertEquals(12, maxProductOfThree.solution(new int[]{-3, -2, 2, 2, 1}));
    Assertions.assertEquals(125, maxProductOfThree.solution(new int[]{-5, 5, -5, 4}));
    Assertions.assertEquals(-120, maxProductOfThree.solution(new int[]{-5, -6, -4, -7, -10}));
  }
}
