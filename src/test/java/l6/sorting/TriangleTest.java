package l6.sorting;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

class TriangleTest {

  final Triangle triangle = new Triangle();

  @Test
  void solution() {

    Assertions.assertEquals(1, triangle.solution(new int[]{10, 5, 8}));
    Assertions.assertEquals(1, triangle.solution(new int[]{Integer.MAX_VALUE, Integer.MAX_VALUE, Integer.MAX_VALUE}));
  }
}
