package l3.timecomplexity;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.MethodSource;

class TapeEquilibriumTest {

  TapeEquilibrium tape = new TapeEquilibrium();

  @Test
  void solution() {
    Assertions.assertEquals(1, tape.solution(new int[]{3, 1, 2, 4, 3}));
    Assertions.assertEquals(0, tape.solution(new int[]{2, 2, 2, 2}));
  }

  @ParameterizedTest(name = "{index} => A={0}, result={1}")
  @MethodSource("createData1")
  public void verifySolution(int [] A, int result) {
    Assertions.assertEquals(result, tape.solution(A));
  }

  private static Object [][] createData1() {
    return new Object [][] {
      new Object [] { new int [] {  3,    1,    2,    4,   3 }, 1 },
      new Object [] { new int [] { -3,    1,    2,   -4,   3 }, 1 },
      new Object [] { new int [] {        5,    2,    7,  10 }, 4 },
      new Object [] { new int [] {    -1000, 1000, -500, 990 }, 490 },
      new Object [] { new int [] {                    1,   2 }, 1 },
      new Object [] { new int [] {                  100, -25 }, 125 },
    };
  }
}
