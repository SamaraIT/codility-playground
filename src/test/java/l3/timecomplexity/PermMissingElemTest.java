package l3.timecomplexity;


import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.stream.Stream;

class PermMissingElemTest {

  PermMissingElem permMissingElem = new PermMissingElem();

  @Test
  void emtpy_array() {
    int[] array = {};
    int solution = permMissingElem.solution(array);
    Assertions.assertEquals(1, solution);
  }

  @Test
  void solution_1() {
    int[] array = {2};
    int solution = permMissingElem.solution(array);
    Assertions.assertEquals(1, solution);
  }

  @Test
  void solution_4() {
    int[] array = {2, 3, 1, 5};
    int solution = permMissingElem.solution(array);
    Assertions.assertEquals(4, solution);
  }

  @ParameterizedTest(name = "{index} => A={0}, result={1}")
  @MethodSource("createData1")
  //@MethodSource("testData") // both versions work!
  public void verifySolution(int [] A, int result) {
    Assertions.assertEquals(result, permMissingElem.solution(A));
  }

  private static Object [][] createData1() {
    return new Object [][] {
      new Object [] { new int [] {                        }, 1 },
      new Object [] { new int [] {                      1 }, 2 },
      new Object [] { new int [] {                      2 }, 1 },
      new Object [] { new int [] {                   1, 3 }, 2 },
      new Object [] { new int [] {                   2, 3 }, 1 },
      new Object [] { new int [] {                1, 2    }, 3 },
      new Object [] { new int [] {                1, 3, 4 }, 2 },
      new Object [] { new int [] {                2, 4, 3 }, 1 },
      new Object [] { new int [] {             2, 3, 1, 5 }, 4 },
      new Object [] { new int [] { 4, 2, 3, 1, 5, 6, 8, 9 }, 7 },
      new Object [] { new int [] { 4, 2, 3, 1, 5, 6, 8, 7 }, 9 },
    };
  }

  static Stream<Arguments> testData() {
    return Stream.of(
      Arguments.of(new int [] {                        }, 1),
      Arguments.of(new int [] {                      1 }, 2),
      Arguments.of(new int [] {                      2 }, 1),
      Arguments.of(new int [] {                   1, 3 }, 2),
      Arguments.of(new int [] {                   2, 3 }, 1),
      Arguments.of(new int [] {                1, 2    }, 3),
      Arguments.of(new int [] {                1, 3, 4 }, 2),
      Arguments.of(new int [] {                2, 4, 3 }, 1),
      Arguments.of(new int [] {             2, 3, 1, 5 }, 4),
      Arguments.of(new int [] { 4, 2, 3, 1, 5, 6, 8, 9 }, 7)
    );
  }


}
