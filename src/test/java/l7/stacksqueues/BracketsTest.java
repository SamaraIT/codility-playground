package l7.stacksqueues;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.MethodSource;

class BracketsTest {
  Brackets brackets = new Brackets();

  //For example, given S = "{[()()]}", the function should return 1 and given S = "([)()]", the function should return 0, as explained above.
  @Test
  void solution() {
    Assertions.assertEquals(1, brackets.solution("{[()()]}"));
    Assertions.assertEquals(0, brackets.solution("([)()]"));
  }

  @ParameterizedTest(name = "{index} => A={0}, result={1}")
  @MethodSource("createData1")
  public void verifySolution(String A, int result) {
    Assertions.assertEquals(result, brackets.solution(A));
  }


  public static Object [][] createData1() {
    return new Object [][] {
      new Object [] { "{[()()]}", 1 },
      new Object [] { "{[(){([])}()]}", 1 },
      new Object [] { "{}", 1 },
      new Object [] { "()", 1 },
      new Object [] { "[]", 1 },
      new Object [] { "[([][])()]", 1 },
      new Object [] { "())", 0 },
      new Object [] { "(()", 0 },
      new Object [] { "([)()]", 0 },
      new Object [] { "(]", 0 },
      new Object [] { "", 1 },
      new Object [] { "{{{{", 0 },
      new Object [] { "))", 0 },
    };
  }
}
