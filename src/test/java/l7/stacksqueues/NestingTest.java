package l7.stacksqueues;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class NestingTest {
  Nesting nesting = new Nesting();

    @Test
    void solution() {
      Assertions.assertEquals(1, nesting.solution("(()(())())"));
      Assertions.assertEquals(1, nesting.solution("()"));
      Assertions.assertEquals(1, nesting.solution(""));
      Assertions.assertEquals(0, nesting.solution(")"));
      Assertions.assertEquals(0, nesting.solution("())"));
      Assertions.assertEquals(0, nesting.solution("(()"));
    }
}
