package l7.stacksqueues;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.MethodSource;

class FishTest {
  Fish fish = new Fish();

  @Test
  void solution() {
    Assertions.assertEquals(2, fish.solution(new int[]{3, 4}, new int[]{1, 1}));
    Assertions.assertEquals(2, fish.solution(new int[]{3, 4}, new int[]{0, 0}));
    Assertions.assertEquals(2, fish.solution(new int[]{3, 4}, new int[]{0, 1}));
    Assertions.assertEquals(1, fish.solution(new int[]{3, 4}, new int[]{1, 0}));
    Assertions.assertEquals(1, fish.solution(new int[]{4, 3, 2, 1, 5}, new int[]{1, 1, 1, 1, 0}));
  }

  @ParameterizedTest(name = "{index} => A={0}, B={1}, result={2}")
  @MethodSource("createData1")
  public void verifySolution(int[] A, int[] B, int result) {
    Assertions.assertEquals(result, fish.solution(A, B));
  }


  static Object[][] createData1() {
    return new Object[][]{
      //new Object[]{new int[]{4, 3, 2, 1, 5}, new int[]{1, 0, 1, 0, 1}, 3},
      //new Object[]{new int[]{4, 3, 2, 0, 5}, new int[]{0, 1, 0, 0, 0}, 2},
      //new Object[]{new int[]{4, 3, 2, 1, 5}, new int[]{0, 1, 0, 0, 0}, 2},
      new Object[]{new int[]{4, 3, 2, 1, 5}, new int[]{0, 1, 1, 0, 0}, 2},
      new Object[]{new int[]{4, 3, 2, 5, 6}, new int[]{1, 0, 1, 0, 1}, 2},
      new Object[]{new int[]{7, 4, 3, 2, 5, 6}, new int[]{0, 1, 1, 1, 0, 1}, 3},
      new Object[]{new int[]{3, 4, 2, 1, 5}, new int[]{1, 0, 0, 0, 0}, 4},
      new Object[]{new int[]{3}, new int[]{1}, 1},
      new Object[]{new int[]{3}, new int[]{0}, 1},
    };
  }
}
