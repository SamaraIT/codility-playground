package l4.countingelements;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.MethodSource;

class MissingIntegerTest {

  MissingInteger mi = new MissingInteger();

  @Test
  void solution() {
    Assertions.assertEquals(2, mi.solution(new int[]{1}));
    Assertions.assertEquals(1, mi.solution(new int[]{78}));
    Assertions.assertEquals(1, mi.solution(new int[]{-1, -3}));

    Assertions.assertEquals(5, mi.solution(new int[]{1, 3, 6, 4, 1, 2}));
    Assertions.assertEquals(4, mi.solution(new int[]{1, 3, 2}));
  }

  @ParameterizedTest(name = "{index} => A={0}, result={1}")
  @MethodSource("createData1")
  public void verifySolution( int [] A, int result) {
    Assertions.assertEquals(result, mi.solution( A));
  }


  private static Object [][] createData1() {
    return new Object [][] {
      new Object [] { new int [] {              1, 3, 6, 4, 1, 2 }, 5 },
      new Object [] { new int [] {           1, 3, 6, 4, 1, 2, 5 }, 7 },
      new Object [] { new int [] {                       1, 2, 3 }, 4 },
      new Object [] { new int [] {                         -1,-3 }, 1 },
      new Object [] { new int [] {                      -1,-3, 2 }, 1 },
      new Object [] { new int [] {                      -1,-3, 1 }, 2 },
      new Object [] { new int [] {                             0 }, 1 },
      new Object [] { new int [] {                      -1000000 }, 1 },
      new Object [] { new int [] {             -1000000,       1 }, 2 },
      new Object [] { new int [] {                       1000000 }, 1 },
      new Object [] { new int [] {       999999, 999998, 1000000 }, 1 },
      new Object [] { new int [] { 1, 3, 999999, 999998, 1000000 }, 2 },
    };
  }
}
