package l4.countingelements;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.MethodSource;

class PermCheckTest {

  PermCheck permCheck = new PermCheck();

  @Test
  void solution() {
    Assertions.assertEquals(1, permCheck.solution(new int[]{4, 1, 3, 2}));
    Assertions.assertEquals(0, permCheck.solution(new int[]{1, 4, 2}));
  }

  @ParameterizedTest(name = "{index} => A={0}, result={1}")
  @MethodSource("createData1")
  public void verifySolution( int [] A, int result) {
    Assertions.assertEquals(result, permCheck.solution( A));
  }

  private static Object [][] createData1() {
    return new Object [][] {
      new Object [] { new int [] {                1 }, 1 },
      new Object [] { new int [] {                2 }, 0 },
      new Object [] { new int [] {             1, 2 }, 1 },
      new Object [] { new int [] {             2, 2 }, 0 },
      new Object [] { new int [] {          1, 3, 2 }, 1 },
      new Object [] { new int [] {          1, 3, 3 }, 0 },
      new Object [] { new int [] {       4, 1, 3, 2 }, 1 },
      new Object [] { new int [] {          4, 1, 3 }, 0 },
      new Object [] { new int [] {    1, 3, 5, 4, 2 }, 1 },
      new Object [] { new int [] { 1, 3, 6, 4, 1, 2 }, 0 },
      new Object [] { new int [] { 1, 3, 5, 4, 6, 2 }, 1 },
      new Object [] { new int [] {          1000000 }, 0 },
      new Object [] { new int [] {       1, 1000000 }, 0 },
    };
  }
}
