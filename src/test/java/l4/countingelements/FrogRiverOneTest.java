package l4.countingelements;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.MethodSource;

class FrogRiverOneTest {

  FrogRiverOne one = new FrogRiverOne();

  @Test
  void solution() {
    Assertions.assertEquals(6, one.solution(5, new int[]{1, 3, 1, 2, 4, 3, 5, 4}));
    Assertions.assertEquals(-1, one.solution(5, new int[]{1, 3, 1, 2, 4, 3, 2, 4}));
  }


  @ParameterizedTest(name = "{index} => X={0}, A={1}, result={2}")
  @MethodSource("createData1")
  public void verifySolution(int X, int [] A, int result) {
    Assertions.assertEquals(result, one.solution(X, A));
  }

  private static Object [][] createData1() {
    return new Object [][] {
      new Object [] { 5, new int [] {             1, 3, 1, 4, 2, 3, 5, 4 },  6 },
      new Object [] { 3, new int [] {                               1, 3 }, -1 }, //never gets across
      new Object [] { 2, new int [] {                         1, 1, 1, 1 }, -1 }, //never gets across
      new Object [] { 3, new int [] {                         1, 2, 2, 3 },  3 },
      new Object [] { 2, new int [] {                         1, 1, 2, 3 },  2 },
      new Object [] { 4, new int [] { 1, 2, 3, 2, 3, 3, 1, 2, 2, 4, 2, 1 },  9 },
      new Object [] { 4, new int [] { 1, 2, 3, 2, 3, 3, 1, 2, 4, 4, 2, 1 },  8 },
      new Object [] { 4, new int [] { 1, 2, 3, 4, 3, 3, 1, 2, 4, 4, 2, 1 },  3 },
    };
  }
}
