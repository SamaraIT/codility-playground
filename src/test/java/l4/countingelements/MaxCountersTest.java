package l4.countingelements;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.MethodSource;

class MaxCountersTest {

  MaxCounters maxCounters = new MaxCounters();

  @Test
  void solution() {
    Assertions.assertArrayEquals(new int[]{3, 2, 2, 4, 2}, maxCounters.solution(5, new int[]{3, 4, 4, 6, 1, 4, 4}));
  }

  @ParameterizedTest(name = "{index} => N={0}, A={1}, result={2}")
  @MethodSource("createData1")
  public void solutionVerify(int N, int[] A, int[] result) {
    Assertions.assertArrayEquals(result, maxCounters.solution(N, A));
  }


  private static Object[][] createData1() {
    return new Object[][]{
      new Object[]{5, new int[]{3, 4, 4, 6, 1, 4, 4}, new int[]{3, 2, 2, 4, 2}},
      new Object[]{3, new int[]{2, 1, 2}, new int[]{1, 2, 0}},
      new Object[]{3, new int[]{2, 1, 2, 2, 1, 2}, new int[]{2, 4, 0}},
      new Object[]{2, new int[]{2, 1, 3, 2}, new int[]{1, 2}},
      new Object[]{2, new int[]{2, 1, 3, 2, 3, 1, 2}, new int[]{3, 3}},
      new Object[]{1, new int[]{2, 1, 2}, new int[]{1}},
      new Object[]{1, new int[]{1, 1, 1, 2, 1, 1, 1, 4, 4}, new int[]{6}},
    };
  }
}
